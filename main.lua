-- see the prints right away
io.stdout:setvbuf("no")

local tick = require "tick"
require "arena"
require "player"
require "bullet"
require "enemy"

local player = Player()
local arena = Arena()
local bullets = {}
local enemies = {}
local score = 0
local difficulty = 1


function checkCollision(a, b)
    local a_left = a.x
    local a_right = a.x + a.w
    local a_top = a.y
    local a_bottom = a.y + a.h
    local b_left = b.x
    local b_right = b.x + b.w
    local b_top = b.y
    local b_bottom = b.y + b.h
    return a_right > b_left
       and a_left < b_right
       and a_bottom > b_top
       and a_top < b_bottom
end


function applyDifficulty()
    if enemySpawner then
        tick.remove(enemySpawner)
    end
    enemySpawner = tick.recur(
        function() table.insert(enemies, Enemy(arena)) end,
        difficulty
    )
end


function love.load()
    playerImage = love.graphics.newImage("player.png")
    enemyImage = love.graphics.newImage("enemy.png")
    applyDifficulty()
end


function love.keypressed(key)
    if key == "space" then
        table.insert(bullets, Bullet(player))
    elseif key == "q" or key == "escape" then
        love.event.quit()
    end
end


function love.update(dt)
    tick.update(dt)
    -- player
    player:update(arena, dt)
    -- bullets
    for i, bullet in ipairs(bullets) do
        bullet:update(dt)
        if bullet.x > arena.x + arena.w - bullet.w then
            table.remove(bullets, i)
        end
    end
    -- enemies
    for i, enemy in ipairs(enemies) do
        enemy:update(dt)
        if enemy.x < arena.x then
            table.remove(enemies, i)
            score = score - 1
        end
    end
    -- collisions
    for i, enemy in ipairs(enemies) do
        if checkCollision(player, enemy) then
            score = score - 10
            table.remove(enemies, i)
            break
        end
        for j, bullet in ipairs(bullets) do
            if checkCollision(bullet, enemy) then
                table.remove(bullets, j)
                table.remove(enemies, i)
                score = score + 1
                break
            end
        end
    end
    -- difficulty
    if difficulty ~= 1 and score < 1 then
        difficulty = 1
        applyDifficulty()
        print(difficulty)
    elseif difficulty ~= 1 / score and score > 1 then
        difficulty = 1 / score
        applyDifficulty()
        print(difficulty)
    end
end


function love.draw()
    -- arena
    arena:draw()
    -- player
    player:draw()
    -- bullets
    for i, bullet in ipairs(bullets) do
        bullet:draw()
    end
    -- enemies
    for i, enemy in ipairs(enemies) do
        enemy:draw()
    end
    -- score
    love.graphics.setColor(0, 1, 0)
    love.graphics.print("Score: " .. score, arena.x + arena.w / 2 - 30, 10)
end
