local Object = require "classic"

Enemy = Object.extend(Object)


function Enemy:new(arena)
    self.h = 30
    self.w = 30
    self.speed = math.random(50, 120)
    self.x = arena.x + arena.w - self.w
    self.y = math.random(arena.y, arena.y + arena.h - self.h)
end


function Enemy:update(dt)
    self.x = self.x - self.speed * dt
end


function Enemy:draw()
    love.graphics.setColor(1, 1, 1)
    love.graphics.draw(enemyImage, self.x, self.y)
end
