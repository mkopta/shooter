local Object = require "classic"

Bullet = Object.extend(Object)


function Bullet:new(player)
    self.w = 5
    self.h = 3
    self.speed = 500
    self.x = player.x
    self.y = player.y + player.h / 2
end


function Bullet:update(dt)
    self.x = self.x + self.speed * dt
end


function Bullet:draw()
    love.graphics.setColor(1, 0, 0)
    love.graphics.rectangle("fill", self.x, self.y, self.w, self.h)
end
