local Object = require "classic"

Player = Object.extend(Object)


function Player:new()
    self.x = 250
    self.y = 150
    self.w = 20
    self.h = 20
    self.speed = 400
end


function Player:update(arena, dt)
    if love.keyboard.isDown("left") then
        self.x = self.x - self.speed * dt
    elseif love.keyboard.isDown("right") then
        self.x = self.x + self.speed * dt
    end
    if love.keyboard.isDown("down") then
        self.y = self.y + self.speed * dt
    elseif love.keyboard.isDown("up") then
        self.y = self.y - self.speed * dt
    end
    if self.x > arena.w + arena.x - self.w then
        self.x = arena.w + arena.x - self.w
    elseif self.x < arena.x then
        self.x = arena.x
    end
    if self.y > arena.h + arena.y - self.h then
        self.y = arena.h + arena.y - self.h
    elseif self.y < arena.y then
        self.y = arena.y
    end
end


function Player:draw()
    love.graphics.setColor(1, 1, 1)
    love.graphics.draw(playerImage, self.x, self.y)
end
