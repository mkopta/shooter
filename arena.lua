local Object = require "classic"

Arena = Object.extend(Object)


function Arena:new()
    self.x = 0
    self.y = 0
    self.w = love.graphics.getWidth()
    self.h = love.graphics.getHeight()
end


function Arena:draw()
    love.graphics.setColor(0, 0, 0)
    love.graphics.rectangle("fill", self.x, self.y, self.w, self.h)
    love.graphics.setColor(love.math.colorFromBytes(0, 0, 0))
    love.graphics.rectangle("line", self.x, self.y, self.w, self.h)
end
